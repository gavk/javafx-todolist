package Presenter.ObservableEntities;

import BusinessRules.Entities.Task;

import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;


public class ObservableTask {

  private StringProperty title;
  private StringProperty note;


  public ObservableTask(Task task) {
    this.title = new SimpleStringProperty(task.getTitle());
    this.note = new SimpleStringProperty(task.getNote());
  }


  public ObservableTask(Task.RequiredParameters requiredParameters) {
    this.title = new SimpleStringProperty(requiredParameters.title());
  }
}
