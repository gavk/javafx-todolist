package Presenter;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;


public class GUI_Service extends javafx.application.Application {

  private static boolean initializationDone = false;

  private Stage primaryStage;


  public static void initializeAndStartGUI() {
    GUI_Service.initializeGUI();
    GUI_Service.startGUI();
  }


  private static void initializeGUI() {

    if (GUI_Service.initializationDone) {
      throw new Error("'GUI_Service' initialization done. Unintentional subsequent initialization means algorithmic error.");
    }

    GUI_Service.initializationDone = true;
  }

  private static void startGUI() {

    if (!GUI_Service.initializationDone) {
      throw new Error("The initialization of 'GUI_Service' has not been executed.");
    }

    javafx.application.Application.launch();
  }


  @Override
  public void start(Stage primaryStage) {

    this.primaryStage = primaryStage;
    this.primaryStage.setTitle("TODO List");

    initializeRootLayout();
  }


  private void initializeRootLayout() {

    FXMLLoader FXML_Loader = new FXMLLoader();
    FXML_Loader.setLocation(getClass().getResource("/RootLayout.fxml"));

    AnchorPane rootLayout;

    try {
      rootLayout = FXML_Loader.load();
    } catch (IOException exception) {
      exception.printStackTrace();
      return;
    }

    primaryStage.setScene(new Scene(rootLayout));
    primaryStage.show();
  }
}
