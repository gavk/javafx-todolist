package Presenter.Components.TaskList;

import BusinessRules.Entities.Task;
import Gateways.Tasks.Retrieving.TasksGateway;

import Presenter.ObservableEntities.ObservableTask;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.VBox;

import java.io.IOException;
import java.util.Map;


public class TasksListController {

  /* --- Dependencies ----------------------------------------------------------------------------------------------- */

  public interface Dependencies {
    TasksGateway tasksGateway();
  }

  private static boolean dependenciesSetupDone = false;

  public static void setDependencies(Dependencies dependencies) {

    if (TasksListController.dependenciesSetupDone) {
      throw new Error("'personOverviewController' already has been initialized.");
    }

    TasksListController.tasksGateway = dependencies.tasksGateway();

    TasksListController.dependenciesSetupDone = true;
  }


  /* --- Data ------------------------------------------------------------------------------------------------------- */
  private static TasksGateway tasksGateway;


  /* --- GUI Elements ----------------------------------------------------------------------------------------------- */
  @FXML private VBox rootElement;
  @FXML private final ListView<ObservableTask> tasksListView = new ListView<>();


  @FXML
  private void initialize() {

    if (!TasksListController.dependenciesSetupDone) {
      throw new Error("You need to call 'TasksListController.setDependencies()' before use this controller.");
    }

    ObservableList<ObservableTask> observableTasks = FXCollections.observableArrayList();

    for (Map.Entry<String, Task> pair: TasksListController.tasksGateway.getAll().entrySet()) {
      observableTasks.add(new ObservableTask(pair.getValue()));
    }


    try {
      // Здесь происходит ошибка - Gradle не может найти нужый файл.
      // Однако, прошу Вас не перемещать файл.
      this.rootElement = new FXMLLoader(getClass().getResource("/Presenter/Components/TaskList/TasksList.fxml")).load();
    } catch (IOException exception) {
      System.out.println("Failed to load 'TasksList.fxml' component.");
    }


    // Завершите этот код
    this.rootElement.getChildren().addAll(this.tasksListView);
    this.tasksListView.setItems(observableTasks);
    this.tasksListView.setCellFactory(list -> new TasksListController.TaskCell());
  }


  private static class TaskCell extends ListCell<ObservableTask> {

    @Override
    public void updateItem(ObservableTask observableTask, boolean doesNotRepresentAnyDomainData) {

      super.updateItem(observableTask, doesNotRepresentAnyDomainData);

      // Заполнике TaskCard.f
    }
  }
}
