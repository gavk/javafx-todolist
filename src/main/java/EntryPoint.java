import Presenter.GUI_Service;
import Presenter.Components.TaskList.TasksListController;

import Gateways.Tasks.Retrieving.MockTasksGateway;


public class EntryPoint {

  public static void main (String[] args) {

    TasksListController.setDependencies(MockTasksGateway::new);

    GUI_Service.initializeAndStartGUI();
  }
}
