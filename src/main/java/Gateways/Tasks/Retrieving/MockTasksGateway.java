package Gateways.Tasks.Retrieving;


import BusinessRules.Entities.Task;

import java.util.AbstractMap;
import java.util.Map;


public class MockTasksGateway extends TasksGateway {

  @Override
  public Map<String, Task> getAll() {
    return allTasks;
  }

  @Override
  public Task getByID(String ID) {

    Task desiredTask = this.allTasks.get(ID);

    if (desiredTask == null) {
      throw new Error(String.format("The task with ID '%s' not found.", ID));
    }

    return desiredTask;
  }

  @Override
  public void add(Task task) {
    this.allTasks.put(task.getID(), task);
  }


  private final Map<String, Task> allTasks = Map.ofEntries(

      new AbstractMap.SimpleEntry<>(
          MockTasksGateway.generateID(),
          new Task(new Task.RequiredParameters() {
            public String ID() { return MockTasksGateway.currentID; }
            public String title() { return "Get up"; }
          })
      ),

      new AbstractMap.SimpleEntry<>(
          MockTasksGateway.generateID(),
          new Task(
            new Task.RequiredParameters() {
              public String ID() { return MockTasksGateway.currentID; }
              public String title() { return "Wash the face"; }
            }
          )
              .setNote("With cold water.")
      ),

      new AbstractMap.SimpleEntry<>(
          MockTasksGateway.generateID(),
          new Task(
              new Task.RequiredParameters() {
                public String ID() { return MockTasksGateway.currentID; }
                public String title() { return "Clean the teeth"; }
              }
          )
              .setNote("At least 5 minutes.")
      ),

      new AbstractMap.SimpleEntry<>(
          MockTasksGateway.generateID(),
          new Task(
              new Task.RequiredParameters() {
                public String ID() { return MockTasksGateway.currentID; }
                public String title() { return "Take the breakfast"; }
              }
          )
              .setNote("Toast, edd and beacon.")
      )
  );


  private static int counterForID_Generation = -1;
  private static String currentID = "-1";

  private static String generateID() {
    MockTasksGateway.counterForID_Generation++;
    MockTasksGateway.currentID = String.valueOf(MockTasksGateway.counterForID_Generation);
    return MockTasksGateway.currentID;
  }
}
