package Gateways.Tasks.Retrieving;

import BusinessRules.Entities.Task;

import java.util.Map;


public abstract class TasksGateway {

  public abstract Map<String, Task> getAll();

  public abstract Task getByID(String ID);

  public abstract void add(Task task);
}
